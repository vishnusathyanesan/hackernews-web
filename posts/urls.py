from django.urls import path

from . import views

urlpatterns = [
    path('addPost/', views.addPost),
    path('posts/', views.posts),
    path('addComment/', views.addComment),
    path('deletePost/', views.deletePost),
]
