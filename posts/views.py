from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from posts.models import Post,Comments
from django.contrib.auth.decorators import login_required

@csrf_exempt
@login_required
def addPost(request):
    data = json.loads(request.body)
    if(data['post']):
        post = Post.objects.get(id=data['post'])
        post.title = data['title']
        post.url = data['url']
        post.text = data['text']
        post.save()
    else:
        post = Post(title=data['title'],url=data['url'],text=data['text'],user=request.user)
        post.save()
    posts = Post._meta.model.objects.all().order_by('-created_at')
    allPosts = []
    for i in posts:
        temp = {}
        temp['id'] = i.id
        temp['title']=i.title
        temp['url']=i.url
        temp['text']=i.text
        temp['created_at']=i.created_at
        temp['user_id']=i.user_id
        temp['user_name']=i.user.username
        temp['edit'] = (request.user == i.user)
        temp['comments'] = serializers.serialize('json', Comments.objects.filter(post=i),use_natural_foreign_keys=True)
        allPosts.append(temp)
    return JsonResponse(allPosts, safe=False)

def posts(request):
    posts = Post.objects.all().order_by('-created_at')
    allPosts = []
    for i in posts:
        temp = {}
        temp['id'] = i.id
        temp['title']=i.title
        temp['url']=i.url
        temp['text']=i.text
        temp['created_at']=i.created_at
        temp['user_id']=i.user_id
        temp['user_name']=i.user.username
        temp['edit'] = (request.user == i.user)
        temp['comments'] = serializers.serialize('json', Comments.objects.filter(post=i),use_natural_foreign_keys=True)
        allPosts.append(temp)
    # response = serializers.serialize('json', allPosts,use_natural_foreign_keys=True)
    return JsonResponse(allPosts, safe=False)

@csrf_exempt
@login_required
def addComment(request):
    data = json.loads(request.body)
    print(data)
    posts = Post.objects.filter(id = data['post'])
    all = Post.objects.all().order_by('-created_at')
    print(posts)
    comment = Comments(user=request.user,post=posts[0],comment=data['comment'])
    comment.save()
    allPosts = []
    for i in all:
        temp = {}
        temp['id'] = i.id
        temp['title']=i.title
        temp['url']=i.url
        temp['text']=i.text
        temp['created_at']=i.created_at
        temp['user_id']=i.user_id
        temp['user_name']=i.user.username
        temp['edit'] = (request.user == i.user)
        temp['comments'] = serializers.serialize('json', Comments.objects.filter(post=i),use_natural_foreign_keys=True)
        allPosts.append(temp)
    # response = serializers.serialize('json', allPosts,use_natural_foreign_keys=True)
    return JsonResponse(allPosts, safe=False)


@csrf_exempt
@login_required
def deletePost(request):
    data = json.loads(request.body)
    posts = Post.objects.filter(id = data['id']).delete()
    all = Post.objects.all().order_by('-created_at')
    allPosts = []
    for i in all:
        temp = {}
        temp['id'] = i.id
        temp['title']=i.title
        temp['url']=i.url
        temp['text']=i.text
        temp['created_at']=i.created_at
        temp['user_id']=i.user_id
        temp['user_name']=i.user.username
        temp['edit'] = (request.user == i.user)
        temp['comments'] = serializers.serialize('json', Comments.objects.filter(post=i),use_natural_foreign_keys=True)
        allPosts.append(temp)
    return JsonResponse(allPosts, safe=False)
