## Requirements
    * python - version > 3
    * django - version 2.1.3
    * pyscopg2 - version 2.7.6.1

## Installation
    * npm install

## Migrate database
    * python manage.py migrate
    
## Run Application
    * python manage.py runserver

## React Build command
    * npm run dev
    
## Application URL
    * Uses Django default port 8000
    * http://localhost:8000