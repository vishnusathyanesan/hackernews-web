import React, { Component } from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import Notifications, {notify} from 'react-notify-toast';

ReactDOM.render(
        <React.Fragment>
					<Notifications />
					<App />
				</React.Fragment>,
  document.getElementById('app')
);
