import React, { Component } from "react";
import ReactDOM from "react-dom";
import Modal from 'react-modal';
import Notifications, {notify} from 'react-notify-toast';
import Cookies from 'universal-cookie';

let successColor = { background: 'green', text: "#FFFFFF" };
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    width                 : '600px'
  }
};
Modal.setAppElement('#app')
class App extends Component {
  constructor() {
    super();
    const cookies = new Cookies();
    let username = cookies.get('username');
    let session_id = cookies.get('session_id');
    let is_authenticated = (username && session_id) ? true:false;
    this.state = {
      modalIsOpen: false,
      signupShow:false,
      username:username,
      session_id:session_id,
      is_authenticated:is_authenticated,
      modalNoteIsOpen:false,
      allPosts:[],
      currentPost:{}
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.loginShow = this.loginShow.bind(this);
    this.submitNote = this.submitNote.bind(this);
    this.closeModalNote = this.closeModalNote.bind(this);
    this.submitPost = this.submitPost.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  openModal() {
    this.setState({modalIsOpen: true});
  }
  submitNote() {
    this.setState({modalNoteIsOpen: true});
  }
  closeModal() {
    this.setState({modalIsOpen: false});
  }
  closeModalNote() {
    this.setState({modalNoteIsOpen: false});
  }
  loginShow() {
    this.setState({signupShow: !this.state.signupShow});
  }
  editPost = value => e =>{
    e.preventDefault();
    var title = value['title'];
    var url = value['url'];
    var text = value['text'];
    var post = value['id'];
    var current = {title:title,url:url,text:text,post:post};
    console.log(title,url,text,post);
    this.setState({currentPost: current});
    this.setState({modalNoteIsOpen: true});
  }
  deletePost = value => e=>{
    e.preventDefault();
    var dataSubmited = { id : value['id']}
    const conf = {
      method: "post",
      body: JSON.stringify(dataSubmited),
      headers: new Headers({ "Content-Type": "application/json" })
    };
    fetch('deletePost/', conf)
    .then(response => response.json())
    .then(response => {
      notify.show('Post deleted successfully','custom',5000, successColor);
      this.setState({ allPosts: response })
    });
  }
  submitComment = value => e =>{
    e.preventDefault();
    var form = e.target;
    var comment = form.elements['comment'].value;
    var user = value.user_id;
    var post = value.id;
    console.log(value,comment);
    var dataSubmited = { comment : comment, user: user, post:post}
    const conf = {
      method: "post",
      body: JSON.stringify(dataSubmited),
      headers: new Headers({ "Content-Type": "application/json" })
    };
    fetch('addComment/', conf)
    .then(response => response.json())
    .then(response => {
      notify.show('Comment added successfully','custom',5000, successColor);
      this.setState({ allPosts: response })
    });
  }
  submitPost = e=>{
    e.preventDefault();
    var form = e.target;
    var title = form.elements['title'].value;
    var url = form.elements['url'].value;
    var text = form.elements['text'].value;
    console.log(form.elements['post'].value);
    var post = form.elements['post'].value;
    if(text && url){
      notify.show('Please remove url or text','custom',5000, successColor);
    }
    else{
      var dataSubmited = { title : title, url: url, text:text, post:post};
      const conf = {
        method: "post",
        body: JSON.stringify(dataSubmited),
        headers: new Headers({ "Content-Type": "application/json" })
      };
      fetch('addPost/', conf)
      .then(response => response.json())
      .then(response => {
        notify.show('Post submitted successfully','custom',5000, successColor);
        this.setState({ allPosts: response });
        this.setState({modalNoteIsOpen: false});
      });
    }
  }
  signup = e => {
    e.preventDefault();
    var form = e.target;
    var dataSubmited = { username : form.elements['username'].value, password: form.elements['password'].value}
    const conf = {
      method: "post",
      body: JSON.stringify(dataSubmited),
      headers: new Headers({ "Content-Type": "application/json" })
    };
    fetch('signup/', conf)
    .then(response => response.json())
    .then(response => {
      notify.show(response.status,'custom',5000, successColor);
    });
  }
  logoutUser = e =>{
    e.preventDefault();
    const cookies = new Cookies();
    cookies.set('username', '', { path: '/' });
    cookies.set('session_id', '', { path: '/' });
    this.setState({is_authenticated: false});
    window.location.href = 'logout/'
  }
  login = e => {
    e.preventDefault();
    var form = e.target;
    var dataSubmited = { username : form.elements['username'].value, password: form.elements['password'].value}
    const conf = {
      method: "post",
      body: JSON.stringify(dataSubmited),
      headers: new Headers({ "Content-Type": "application/json" })
    };
    fetch('login/', conf)
    .then(response => response.json())
    .then(response => {
        const cookies = new Cookies();
        cookies.set('username', response['username'], { path: '/' });
        cookies.set('session_id', response['session_id'], { path: '/' });
        notify.show(response.status,'custom',5000, successColor);
        this.setState({is_authenticated: true});
        this.closeModal();
    });
  }
  handleChange(e) {
    // console.log(e.target.name,e.target.value);
    this.state.currentPost[e.target.name] = e.target.value;
    this.forceUpdate();
  }
  componentWillMount() {
    fetch("posts/")
      .then(response => {
        if (response.status !== 200) {
          return this.setState({ placeholder: "Something went wrong" });
        }
        return response.json();
      })
      .then(data => this.setState({ allPosts: data }));
  }
  render() {
    const { open } = this.state;
    const allPosts = this.state.allPosts;
    console.log(typeof(this.state.allPosts));
		return (
      <div>
        <div className="topnav">
          <a className="active" href="/">Home</a>
          <a onClick={this.submitNote} className={`${this.state.is_authenticated ? '':'d-none'}`}>Submit</a>
          <div className={`login-container ${this.state.is_authenticated ? 'd-none':''}`}>
            <button onClick={this.openModal}>Login</button>
          </div>
          <div className={`login-container ${this.state.is_authenticated ? '':'d-none'}`}>
            <button onClick={this.logoutUser}>Logout</button>
          </div>

          <Modal
            isOpen={this.state.modalNoteIsOpen}
            onRequestClose={this.closeModalNote}
            contentLabel="Add post"
            style={customStyles}
          >
            <form onSubmit={this.submitPost} className={`form-signin form-addpost ${this.state.signupShow ? 'd-none':''}`}>
              <h2 className="form-signin-heading">Add post</h2>
              <input type="text" onChange={this.handleChange} value={this.state.currentPost.title} className="form-control" name="title" placeholder="Title" required="" autoFocus="" />
              <input type="text" onChange={this.handleChange} value={this.state.currentPost.url} className="form-control" name="url" placeholder="Url" required=""/>
              <input type="hidden" onChange={this.handleChange} value={this.state.currentPost.post} className="form-control" name="post" placeholder="post" required=""/>
              or
              <textarea type="text" onChange={this.handleChange} value={this.state.currentPost.text} className="form-control" name="text" placeholder="Text"></textarea>
              <button className="btn btn-sm btn-primary btn-block pull-right" type="submit">Submit</button>
            </form>
          </Modal>
          <Modal
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Login Modal"
          >
            <form onSubmit={this.login} className={`form-signin ${this.state.signupShow ? 'd-none':''}`}>
              <h2 className="form-signin-heading">Please login</h2>
              <input type="text" className="form-control" name="username" placeholder="Email Address" required="" autoFocus="" />
              <input type="password" className="form-control" name="password" placeholder="Password" required=""/>
              <button className="btn btn-sm btn-primary btn-block pull-right" type="submit">Login</button>
              <p className="create-account">
                <span>
                  Create Account <a onClick={this.loginShow}>signup</a>
                </span>
              </p>
            </form>
            <form onSubmit={this.signup} className={`form-signin ${this.state.signupShow ? '':'d-none'}`}>
              <h2 className="form-signin-heading">Please signup</h2>
              <input type="text" className="form-control" name="username" placeholder="Email Address/Username" required="" autoFocus="" />
              <input type="password" className="form-control" name="password" placeholder="Password" required=""/>
              <button className="btn btn-sm btn-primary btn-block pull-right" type="submit">Signup</button>
              <p className="create-account">
                <span>
                  Already have account <a onClick={this.loginShow}>Login</a>
                </span>
              </p>
            </form>
          </Modal>
        </div>
        {
            this.state.allPosts.map(el => (
              <div className="panel-group" key={el.id}>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h4 className="panel-title">
                      <a data-toggle="collapse" href={'#collapse'+el.id} className={el.url ? 'd-none' : ''}>{el.title}</a>
                      <a href={el.url} className={el.url ? '' : 'd-none'}>{el.title}</a>
                      <span className="user-name">By {el.user_name}</span>
                      <a className={`pull-right comments-style ${el.edit ? '' :'d-none' }`} href="" onClick={this.deletePost(el)}>delete</a>
                      <a className={`pull-right comments-style ${el.edit ? '' :'d-none' }`} href="" onClick={this.editPost(el)}>edit</a>
                      <a data-toggle="collapse" className="pull-right comments-style" href={'#collapse'+el.id}>Comments({JSON.parse(el.comments).length})</a>
                      <span className="pull-right user-name date-style">{new Date(el.created_at).toString()}</span>
                    </h4>
                  </div>
                  <div id={'collapse'+el.id} className="panel-collapse collapse">
                    <div className="card-body">
                      {el.text}
                    </div>
                    <hr/>
                    <div className="comment-div">
                      <span>Comments({JSON.parse(el.comments).length})</span>
                      <ul className="list-group">
                        {JSON.parse(el.comments).map(cm => (
                          <li className="list-group-item" key={cm.pk}>
                            <span>{cm.fields.comment}</span>
                            <span className="pull-right comments-style">By {cm.fields.user[0]}</span>
                            <span className="pull-right user-name date-style comment-date">{new Date(cm.fields.created).toString()}</span>
                          </li>
                        ))}

                      </ul>
                      <form onSubmit={this.submitComment(el)} className={`${this.state.is_authenticated ? '':'d-none'}`}>
                        <div className="input-group mb-3">
                            <input name="comment" type="text" className="form-control" placeholder="Comments" aria-label="Comments" aria-describedby="button-addon2"/>
                            <div className="input-group-append">
                              <button type="submit" className="btn btn-outline-secondary" id={"button-addon"+el.id}>Send</button>
                            </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

            ))
        }
      </div>
		)
	}
};
export default App;
