from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import JsonResponse
from django.contrib.auth import authenticate, login,logout
from django.shortcuts import redirect

def index(request):
    return render(request, 'dir/index.html')

@csrf_exempt
def signup(request):
    data = json.loads(request.body)
    checkexists = User.objects.filter(username=data['username'])
    res ={}
    print(len(checkexists))
    if(len(checkexists)>0):
        res['status'] = 'Username already exists'
    else:
        user = User.objects.create_user(data['username'], data['username'], data['password'])
        res['status'] = 'Account created successfully'
    return JsonResponse(res)

@csrf_exempt
def login_user(request):
    data = json.loads(request.body)
    username = data['username']
    password = data['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        res = {'username':username,'session_id':request.session.session_key,'status':'Login successfully'}
    else:
        res = {'status':'Check your credentials'}
    return JsonResponse(res)

@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect('/')
